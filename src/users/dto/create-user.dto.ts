import { IsNotEmpty,MinLength,Matches } from "class-validator";
export class CreateUserDto {
  @IsNotEmpty()
  @MinLength(5)
  login:string;

  @IsNotEmpty()
  @MinLength(5)
  name:string;

  @IsNotEmpty()
  @MinLength(8)
  @Matches(/^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z]).{8,}$/)
  password:string;
}
